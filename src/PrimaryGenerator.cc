//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file PrimaryGenerator.cc
/// \brief Implementation of the PrimaryGenerator1 class
/// \author Zhihong Shen
/// \date August 6th, 2018
//

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "PrimaryGenerator.hh"
#include "G4Event.hh"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGenerator::PrimaryGenerator()
: G4VPrimaryGenerator()
{
  iter = 0;
  AllEvtsTrans = false;
  EachEvtTrans = false;
  zmax = 12530. ;
  zmin = 12505. ;

  // initialize the chain pointer
  chain = new TChain("GiGaGeo.PlaneDet.Tuple/Hits");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGenerator::~PrimaryGenerator()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PrimaryGenerator::AddNewRootFile(std::string new_root_file){
  chain->Add(new_root_file.data());
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PrimaryGenerator::SetAllEvtsTrans(bool allEvtsTrans){
  AllEvtsTrans = allEvtsTrans;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PrimaryGenerator::SetEachEvtTrans(bool eachEvtTrans){
  EachEvtTrans = eachEvtTrans;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PrimaryGenerator::SetZ(double z_min, double z_max){
  zmin = z_min;
  zmax = z_max;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PrimaryGenerator::GeneratePrimaryVertex(G4Event* event)
{
  // Declaration of leaf types
  Int_t           n;
  Float_t         pid[20000];   //[n]
  Float_t         x[20000];   //[n]
  Float_t         y[20000];   //[n]
  Float_t         z[20000];   //[n]
  Float_t         t[20000];   //[n]
  Float_t         px[20000];   //[n]
  Float_t         py[20000];   //[n]
  Float_t         pz[20000];   //[n]
  Float_t         e[20000];   //[n]

  chain->SetBranchAddress("n", &n);
  chain->SetBranchAddress("pid", pid);
  chain->SetBranchAddress("x", x);
  chain->SetBranchAddress("y", y);
  chain->SetBranchAddress("z", z);
  chain->SetBranchAddress("t", t);
  chain->SetBranchAddress("px", px);
  chain->SetBranchAddress("py", py);
  chain->SetBranchAddress("pz", pz);
  chain->SetBranchAddress("e", e);

  // note that the entries of the chain should less than maximum of G4int
  G4int nentries = chain->GetEntries();
  chain->GetEntry( event->GetEventID()%nentries );
//  std::cout << "event:"<<nentries << std::endl;
  std::cout << "event"<<event->GetEventID()%nentries<< std::endl;

  if ( EachEvtTrans && AllEvtsTrans){
    G4cout<<"Do you realy want set both each event transformation and all"<<
              "evnets transformation are true simultaneously? y/n"<<G4endl;
  }

  if ( AllEvtsTrans ){
    scale_x = 0.5 < G4UniformRand() ? 1 : -1 ;
    scale_y = 0.5 < G4UniformRand() ? 1 : -1 ;
  }

  for (G4int nhits=0;nhits<n;nhits++) {
    if(pz[nhits]>0 && z[nhits]< zmax && z[nhits]>zmin){
      if ( EachEvtTrans ){
        scale_x = 0.5 < G4UniformRand() ? 1 : -1 ;
        scale_y = 0.5 < G4UniformRand() ? 1 : -1 ;
      }
      G4PrimaryParticle *pptmp = new G4PrimaryParticle(pid[nhits],
                                                       scale_x * px[nhits],
                                                       scale_y * py[nhits],
                                                       pz[nhits],
                                                       e[nhits]);
      pp.push_back(pptmp);
      G4PrimaryVertex *pvtmp =new G4PrimaryVertex(scale_x * x[nhits],
                                                  scale_y * y[nhits],
                                                  z[nhits],
                                                  t[nhits]);
      pv.push_back(pvtmp);
      pv[iter]->SetPrimary(pp[iter]);
      event->AddPrimaryVertex(pv[iter]);
      iter++;
	  }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
