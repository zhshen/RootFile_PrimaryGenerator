//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file PrimaryGenerator.hh
/// \brief Definition of the PrimaryGenerator class
/// \author Zhihong Shen
/// \date August 6th, 2018
//

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef PrimaryGenerator_h
#define PrimaryGenerator_h 1

#include "G4VPrimaryGenerator.hh"
#include "G4PrimaryParticle.hh"
#include "G4PrimaryVertex.hh"
#include "Randomize.hh"
#include "TFile.h"
#include "TROOT.h"
#include "TTree.h"
#include "TChain.h"
#include <vector>
#include <string>

class G4Event;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

 /*!
 *  \brief     primary generator
 *  \details   This class read Hits from the root file, transform them to be
 *G4PrimaryParticle and G4PrimaryVertex, and add them to exsited event
 *  \author    Zhihong Shen
 *  \date      August 6th, 2018
 */
class PrimaryGenerator : public G4VPrimaryGenerator
{
  public:
    //! A constructor.
    /*!
    *
    *usage:
    *
    *before you use the method GeneratePrimaryVertex(G4Event* event), you should
    *figure out the path of your root file with
    *AddNewRootFile(std::string new_root_file) method.
    *
    *eg:
    *  you should put your AddNewRootFile in the PrimaryGeneratorAction's
    *  constructor. Do not put in GeneratePrimaries(). Otherwise, the chain will
    *  increase every event.(see the PrimaryGeneratorAction class)
    *
    *  fPrimaryGenerator->AddNewRootFile("./mytuple_100ev_01.root");
    *  fPrimaryGenerator->GeneratePrimaryVertex(event);
    *
    */
    PrimaryGenerator();
    //! A destructor.
   ~PrimaryGenerator();

  public:
    virtual void GeneratePrimaryVertex(G4Event*);
    //! the function to add new rootfile
    void AddNewRootFile(std::string new_root_file);
    //! the function to set whether use all events transformation
    void SetAllEvtsTrans(bool allEvtsTrans = true);
    //! the function to set whether use all events transformation
    void SetEachEvtTrans(bool eachEvtTrans = false);
    //! the function to set zmin and zmax
    void SetZ(double z_min = 12505., double z_max =12530.);

  private:
    //! be used to remember the index of G4PrimaryParticle and G4PrimaryVertex
    G4int iter;

    //! be used to record the Primary Particle
    std::vector<G4PrimaryParticle*>  pp;

    //! be used to record the Primary Vertex;
    std::vector<G4PrimaryVertex*>  pv;

    //! the ststus fo all events transformation (true or false)
    bool AllEvtsTrans;

    //! the status of each event transformation (true or false)
    bool EachEvtTrans;

    //! the flag: x,px and y,py is plus or minus
    int scale_x  = 1 ;
    int scale_y  = 1 ;

    //! minimun of z (using to select the hits)
    double zmax;

    //! minimun of z (using to select the hits)
    double zmin;

    //! the chain used to analyse the root files.
    TChain* chain;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
